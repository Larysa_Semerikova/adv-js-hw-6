// JS є однопотоковим, тобто завдання виконуються одне після іншого. 
// Однак бувають ситуації, що перше завдання потребує багато часу на виконання, 
// затримуючи все. Саме в таких випадках необхідна асинхронність: перше завдання
// запускається в фоновому режимі, дозволяючи другому запуститись "без черги".

const btn = document.querySelector(".btn")

btn.addEventListener("click", sendRequest)

async function sendRequest() {
    try{
        const ipRequest = await fetch("https://api.ipify.org/?format=json");
        const ip = await ipRequest.json();
        console.log(ip)
    
        const addressRequest = await fetch(`http://ip-api.com/json/${ip.ip}?fields=continent,country,regionName,city,district`)
        const address = await addressRequest.json()
        console.log(address)
    
        function createList(){
            const wrapper = document.querySelector(".wrapper")
            const list = document.createElement("ul")
            let keys = [`${address.continent}`, `${address.country}`, `${address.regionName}`, `${address.city}`, `${address.district}`]
            for (let key of keys){
                if(key !== ""){
                    const li = document.createElement('li')
                    li.append(key)
                    list.append(li)
                }
            }
            wrapper.append(list)
        }

        createList()

    } catch (error){
        console.log("Sorry: " +error.message)
    }

}

